@extends("client.layouts.master")

@section("content")
    Panel użytkownika - {{ Auth::user()->login }}

    <a href="{{ route("logout") }}">Wyloguj się</a>

    @if(empty($orders))
        <span>Brak zamówień</span>
    @else
        @foreach($orders AS $order)
            <li>
                Numer zamówienia: {{  $order->id }}<br>
                Status:
                @if($order->frozen)
                    Niedokonczone <a href="{{ route("unfrozeOrder", ['order_id' => $order->id])  }}">Realizuj zamówienie</a>
                @endif

                @if($order->completed)
                    Zakonczone
                    <a href="{{ route("userOrder", ['order_id' => $order->id]) }}">Wyświetl szczegóły</a>
                    <a href="{{ route("userPdf", ['order_id' => $order->id]) }}">Generuj pdf</a>
                @endif
            </li>
        @endforeach
    @endif

@endsection