@extends("client.layouts.master")

@section("content")
    Historia zamówienia<br><br>

    Numer zamówienia: {{  $order['id'] }}<br>
    Zamówione produkty:
    <ul>
    @foreach($order['items'] AS $item)
        <li>
            Nazwa produktu: {{$item['name']}} <br>
            Cena produktu: {{$item['price']}} zł <br>
            Ilość produktu: {{$item['quantity']}} <br>
        </li>
    @endforeach
    </ul>
    Dostawca: {{ $order['transport']->name }} <br>
    Cena dostawy: {{ $order['transport']->price }} zł <br>
    Status: Zakończone

@endsection