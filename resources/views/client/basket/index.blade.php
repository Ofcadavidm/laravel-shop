@extends("client.layouts.master")

@section("content")
    Koszyk: Produkty
    @if(count($items))
        <ul>
            @foreach($items AS $item)
                @include("client.partials.item_basket", ['item' => $item])
            @endforeach
        </ul>

        <a href="{{ route("basketAddress")  }}">Ustaw adres</a>
    @else
        <span>Brak produktów w koszyku</span>
    @endif


@endsection