@extends("client.layouts.master")

@section("content")
    Koszyk - Sposób dostawy

    {!! Form::open(array('url' => route('basketTransportPost'), 'method' => 'post', 'class' => 'form', 'role' => 'form', 'id' => 'basketTransport')) !!}
    <div class="form-group">
        {!! csrf_field() !!}

        @foreach($transports AS $index => $transport)
                {!! Form::label("transport_".$index, $transport->name) !!}
                {!! Form::radio('transport', $transport->id, ['id' => "transport_".$index]) !!} <br>

        @endforeach

        @if(isset($customOrderId))
            {!! Form::number("order_id", $customOrderId, ['hidden' => 1]) !!}
        @endif

        {!! Form::submit("Realizuj zamówienie") !!}
    </div>
    {!! Form::close() !!}
@endsection