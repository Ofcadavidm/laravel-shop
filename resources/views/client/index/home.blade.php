@extends("client.layouts.master")

@section("content")
    Wybierz kategorie:
    <ul>
        @foreach($groups AS $group)
            <a href="{{ route('group', ['id' => $group->id]) }}">
                <li>{{$group->name}}</li>
            </a>
        @endforeach
    </ul>

@endsection