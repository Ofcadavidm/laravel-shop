@extends("admin.layouts.master")

@section("content")
    Dodaj produkt

    {!! Form::open(array('url' => route('addItem'), 'method' => 'post', 'class' => 'form', 'role' => 'form')) !!}
        <div class="form-group">
            {!! Form::text('name', null, ["class" => "form-control", "placeholder" => "Nazwa"]) !!}
            {!! Form::number('price', null, ["class" => "form-control", "placeholder" => "Cena", 'min' => 1]) !!}
            {!! Form::number('amount', null, ["class" => "form-control", "placeholder" => "Ilość", 'min' => 1]) !!}
            {!! Form::text('description', null, ["class" => "form-control", "placeholder" => "Opis"]) !!}
            {!! Form::select('group_id', $groups) !!}
            {!! Form::submit('Dodaj') !!}
        </div>
    {!! Form::close() !!}
@endsection