<div class="container-fluid" id="header">
    <span class="name">Panel administracyjny</span>

    <a href="{{ route('home')}}" class="btnMy">
        <i class="fa fa-home"></i>
    </a>

    @if(Auth::check() && Auth::user()->login === "admin")
        <a href="{{ route('adminGroup')}}" class="btnMy">
            <i class="fa fa-archive"></i>
        </a>
        <a href="{{ route('adminItem')}}" class="btnMy">
            <i class="fa fa-map"></i>
        </a>
        <a href="{{ route('user')}}" class="btnMy">
            <i class="fa fa-user"></i>
        </a>
    @endif
</div>