<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionsRequiredMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if (!$user = $request->user()) {
            return $next($request);

        }

        $route = $request->route();
        $actions = $route->getAction();

        if (!$permissions = isset($actions['permissions']) ? $actions['permissions'] : null) {
            return $next($request);
        }

        $userPermissions = [];
        $userPermissionsTemp = $user->permissions()->whereIn('slug', (array) $permissions)->get()->toArray();

        foreach($userPermissionsTemp AS $permission){
            foreach($permission AS $key => $value){
                if($key === "slug"){
                    $userPermissions[] = $value;
                }
            }
        }

        $permissions = (array) $permissions;

        if (isset($actions['permissions_require_all'])) {
            if (count($permissions) == count($userPermissions))
            {
                return $next($request);
            }
        }
        else {
            if (count($userPermissions) >= 1) {
                return $next($request);
            }
        }

        return redirect()->route("adminLogin");
    }
}
