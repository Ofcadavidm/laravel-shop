<?php

namespace App\Http\Controllers\Admin;

use App\Groups;
use App\Http\Controllers\Controller;
use App\Items;
use Request;

class ItemController extends Controller
{
    public function indexAction(){
        $items = Items::all();

        return view("admin.items.index", ['items' => $items]);
    }

    public function getRemove(Request $request, $itemId){
        Items::find($itemId)->delete();

        return redirect()->route("adminItem");
    }

    public function getAdd(){
        $groups = Groups::prepareForSelect();
        return view("admin.items.add", ['groups' => $groups]);
    }

    public function postAdd(){
        $data = Request::all();

        unset($data['_token']);
        Items::create($data);

        return redirect()->route("adminItem");
    }
}
