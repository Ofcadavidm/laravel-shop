<?php

namespace App\Http\Controllers\Admin;

use App\Groups;
use App\Http\Controllers\Controller;
use Request;

class GroupController extends Controller
{
    public function indexAction(){
        $groups = Groups::all();

        return view("admin.group.index", ['groups' => $groups]);
    }

    public function getRemove(Request $request, $orderId){
        Groups::find($orderId)->delete();

        return redirect()->route("adminGroup");
    }

    public function getAdd(){
        return view("admin.group.add");
    }

    public function postAdd(){
        $data = Request::all();

        unset($data['_token']);
        Groups::create($data);

        return redirect()->route("adminGroup");
    }
}
