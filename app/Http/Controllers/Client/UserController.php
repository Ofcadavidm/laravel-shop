<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Orders;
use Auth;
use Route;
use Request;
use mPDF;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function indexAction(){
        $orders = Orders::where("user_id", Auth::user()->id)->get();

        return view("client.user.index", ['orders' => $orders]);
    }

    public function logoutAction(){
        Auth::logout();

        return redirect()->route("home");
    }

    public function orderAction(Request $request, $orderId){
        if($orderId){
            $order = Orders::getOrderData($orderId);
        }
        else{
            abort(404);
        }

        return view("client.user.order", ['order' => $order]);
    }

    public function pdfAction(Request $request, $orderId){
        if($orderId){
            $order = Orders::getOrderData($orderId);
            $mpdf = new mPDF();
            $html = "";

            $html .= "Numer zamówienia: ".$order['id']."<br>";
            $html .= "Zamówione produkty: <ul>";

            foreach($order['items'] AS $item){
                $html .= "<li>
                                Nazwa produktu: ".$item['name']." <br>
                                Cena produktu: ".$item['price']." zł <br>
                                Ilość produktu: ".$item['quantity']." <br>
                            </li></ul>";
            }

            $html .= "Dostawca: ".$order['transport']->name ." <br>";
            $html .= "Cena dostawy: ".$order['transport']->price ."zł <br>";
            $html .= "Dostawca: ".$order['transport']->name ." <br>";
            $html .= "Status: Zakończone";

            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }
        else{
            abort(404);
        }

        exit;
    }
}
