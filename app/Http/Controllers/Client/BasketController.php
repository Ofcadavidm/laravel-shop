<?php

namespace App\Http\Controllers\Client;

use App\Address;
use App\Http\Controllers\Controller;
use App\Items;
use App\OrderItems;
use App\Orders;
use App\Transport;
use Session;
use Request;
use Auth;
use Validator;

class BasketController extends Controller
{
    private $orderId = 0;

    public function __construct(){
        $this->middleware('auth', ['except' => ['getIndex', 'postAddItem', 'postRemoveItem']]);
        $this->orderId = $this->getOrderId();
    }

    public function getIndex(){
        $order = Orders::find($this->orderId);

        if(!$order->completed && !$order->frozen){
            $items = OrderItems::getOrderItems($this->orderId);
            return view("client.basket.index", ['items' => $items]);
        }
        else{
            Session::forget("orderId");
            return redirect("basket");
        }

    }
    public function postAddItem(){
        $id = Request::input("id");
        $quantity = Request::input("quantity");
        $price = Request::input("price");

        $itemInOrder = OrderItems::where("item_id", $id)->where("order_id", $this->orderId)->get();

        if($itemInOrder->count() === 1){
            $itemInOrder = $itemInOrder[0];
        }

        $item = Items::find($id);

        if($itemInOrder->count()){
            if(($item->amount + $itemInOrder['item_quantity']) - $quantity < 0){
                $quantity = $item->amount + $itemInOrder['item_quantity'];
                $difference = 0;
            }
            else{
                $difference = $itemInOrder['item_quantity'] - $quantity;
            }

            OrderItems::where("item_id", $id)->where("order_id", $this->orderId)->update(
                ['item_quantity' => $quantity, 'item_price' => $price]
            );
            Items::where("id", $id)->update(['amount' => $item->amount + $difference]);
        }
        else{
            if($item->amount - $quantity < 0){
                $quantity = $item->amount;
            }

            OrderItems::insert(
                ['item_id' => $id, 'item_quantity' => $quantity, 'item_price' => $price, 'order_id' => $this->orderId]
            );

            Items::where("id", $id)->update(['amount' => $item->amount - $quantity]);
        }

        return redirect()->route("basket");
    }
    public function postRemoveItem(){
        $id = Request::input("id");
        $item = Items::find($id);
        $orderItem = OrderItems::where("item_id", $id)->where("order_id", $this->orderId)->get()[0];

        OrderItems::where("item_id", $id)->where("order_id", $this->orderId)->delete();
        Items::where("id", $id)->update(['amount' => $item->amount + $orderItem['item_quantity']]);

        return redirect()->route("basket");
    }
    public function getAddress(){
        return view("client.basket.address");
    }
    public function postAddress(){
        $validator = Validator::make(Request::all(), [
            'name' => 'required',
            'surname' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route("basketAddress")
                ->withErrors($validator)
                ->withInput();
        }

        $data = Request::all();
        unset($data['_token']);

        $address = Address::create($data);
        Orders::where("id", $this->orderId)->update(
            ['address_id' => $address->id, 'user_id' => Auth::user()->id]
        );

        return redirect()->route("basketTransport");
    }
    public function getTransport(){
        $transports = Transport::all();

        return view("client.basket.transport", ['transports' => $transports]);
    }
    public function postTransport(){
        $data = Request::all();

        if(isset($data['transport'])){
            if(isset($data['order_id'])){
                $orderId = $data['order_id'];
            }
            else{
                $orderId = $this->orderId;
            }

            Orders::where("id" ,$orderId)->update(['transport_id' => $data['transport'], 'completed' => 1, 'frozen' => 0]);

        }

        return redirect()->route("user");
    }
    public function postFrozeOrder(){
        $validator = Validator::make(Request::all(), [
            'name' => 'required',
            'surname' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route("basketAddress")
                ->withErrors($validator)
                ->withInput();
        }

        $data = Request::all();
        unset($data['_token']);

        $address = Address::create($data);

        Orders::where("id", $this->orderId)->update(
            ['address_id' => $address->id, 'frozen' => 1, 'user_id' => Auth::user()->id]
        );

        Session::forget("orderId");
        return redirect()->route("home");
    }
    public function getUnfrozeOrder(Request $request, $orderId){
        $order = Orders::find($orderId);
        $transports = Transport::all();

        if(!$order->completed && $order->frozen && $order->user_id === Auth::user()->id){
            return view("client.basket.transport", ['transports' => $transports, 'customOrderId' => $orderId]);
        }
        else{
            return redirect()->route("user");
        }
    }

    private function getOrderId(){
        if (!Session::has('orderId')) {
            Session::put("orderId", Orders::getNewOrderId());
            Session::save();
        }

        return Session::get("orderId");
    }
}
