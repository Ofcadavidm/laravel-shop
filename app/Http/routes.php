<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/', ["as" => "home", "uses" => "Client\\IndexController@homeAction"]);
Route::get('/group/{id}', ["as" => "group", "uses" => "Client\\IndexController@groupView"]);

Route::get('/search', ["as" => "search", "uses" => "Client\\SearchController@searchForm"]);
Route::get('/search-results', ["as" => "searchResults", "uses" => "Client\\SearchController@searchResults"]);

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/item/{id}', ["as" => "item", "uses" => "Client\\IndexController@itemView"]);

    Route::get('/basket', ["as" => "basket", "uses" => "Client\\BasketController@getIndex"]);
    Route::post('/basket/add', ["as" => "addToBasket", "uses" => "Client\\BasketController@postAddItem"]);
    Route::post('/basket/remove', ["as" => "removeFromBasket", "uses" => "Client\\BasketController@postRemoveItem"]);
    Route::get('/basket/address', ["as" => "basketAddress", "uses" => "Client\\BasketController@getAddress"]);
    Route::post('/basket/address', ["as" => "basketAddressPost", "uses" => "Client\\BasketController@postAddress"]);
    Route::get('/basket/transport', ["as" => "basketTransport", "uses" => "Client\\BasketController@getTransport"]);
    Route::post('/basket/transport', ["as" => "basketTransportPost", "uses" => "Client\\BasketController@postTransport"]);
    Route::post('/basket/froze-order', ["as" => "frozeOrder", "uses" => "Client\\BasketController@postFrozeOrder"]);
    Route::get('/basket/unfroze-order/{order_id}', ["as" => "unfrozeOrder", "uses" => "Client\\BasketController@getUnfrozeOrder"]);

    Route::get('/user', ["as" => "user", "uses" => "Client\\UserController@indexAction"]);
    Route::get('/user/order/{order_id}', ["as" => "userOrder", "uses" => "Client\\UserController@orderAction"]);
    Route::get('/user/pdf/{order_id}', ["as" => "userPdf", "uses" => "Client\\UserController@pdfAction"]);
    Route::get('/user/logout', ["uses" => "Client\\UserController@logoutAction", 'as' => 'logout']);
    Route::controller("/user", "Client\\AuthController");

    Route::get('/admin', ['middleware' => ['auth', 'permissions.required'], 'as' => 'admin','permissions' => 'admin', 'uses' => 'Admin\\IndexController@homeAction']);
    Route::get('/admin/login', ['uses' => 'Admin\\LoginController@indexAction', 'as' => 'adminLogin']);

    Route::get('/admin/group', ['middleware' => ['auth', 'permissions.required'], 'as' => 'adminGroup','permissions' => 'admin', 'uses' => 'Admin\\GroupController@indexAction']);
    Route::get('/admin/group/remove/{group_id}', ['middleware' => ['auth', 'permissions.required'], 'as'=> 'removeGroup','permissions' => 'admin', 'uses' => 'Admin\\GroupController@getRemove']);
    Route::get('/admin/group/add', ['middleware' => ['auth', 'permissions.required'], 'as'=> 'addGroup','permissions' => 'admin', 'uses' => 'Admin\\GroupController@getAdd']);
    Route::post('/admin/group/add', ['middleware' => ['auth', 'permissions.required'], 'as'=> 'addGroupPost','permissions' => 'admin', 'uses' => 'Admin\\GroupController@postAdd']);

    Route::get('/admin/item', ['middleware' => ['auth', 'permissions.required'], 'as' => 'adminItem','permissions' => 'admin', 'uses' => 'Admin\\ItemController@indexAction']);
    Route::get('/admin/item/remove/{item_id}', ['middleware' => ['auth', 'permissions.required'], 'as'=> 'removeItem','permissions' => 'admin', 'uses' => 'Admin\\ItemController@getRemove']);
    Route::get('/admin/item/add', ['middleware' => ['auth', 'permissions.required'], 'as'=> 'addItem','permissions' => 'admin', 'uses' => 'Admin\\ItemController@getAdd']);
    Route::post('/admin/item/add', ['middleware' => ['auth', 'permissions.required'], 'as'=> 'addItemPost','permissions' => 'admin', 'uses' => 'Admin\\ItemController@postAdd']);
});