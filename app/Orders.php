<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Orders extends Model
{
    protected $table = "orders";
    protected $primaryKey = "id";

    public static function getNewOrderId(){
        $newOrder = Orders::create();

        return $newOrder->id;
    }

    public static function getOrderData($orderId){
        $orderData = [];
        $order = Orders::where("user_id", Auth::user()->id)->where("id", $orderId)->get();
        $orderItems = OrderItems::getOrderItems($orderId);

        if(count($order)){
            $order = $order[0]['attributes'];

            foreach($order AS $key => $value){
                if($key === "transport_id"){
                    $orderData['transport'] = Transport::where("id", $order['transport_id'])->get()[0];
                    continue;
                }
                $orderData[$key] = $value;
            }

            foreach($orderItems AS $orderItem) {
                $orderData['items'][] = $orderItem['attributes'];
            }
        }

        return $orderData;
    }
}
