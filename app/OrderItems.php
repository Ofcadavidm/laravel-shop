<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $table = "order_items";
    protected $casts = [
        'quantity' => 'integer',
        'price' => 'float'
    ];

    public static function getOrderItems($orderId = 0){
        $orderItems = OrderItems::where("order_id", $orderId)->get();
        $items = [];

        foreach($orderItems AS $orderItem){
            $item = Items::find($orderItem->item_id);
            $item['quantity'] = $orderItem->item_quantity;
            $item['price'] = $orderItem->item_price;

            $items[] = $item;
        }

        return $items;
    }
}
